#include "Helper.h"
#include <direct.h>
#include <fstream>  
#include <filesystem>

using namespace std::experimental::filesystem::v1;

typedef int (WINAPI* myFunc)();
void Helper::secretA(char* name)
{
	HMODULE dll = LoadLibrary("secret.dll");
	if (dll != NULL) {
		myFunc func = (myFunc)GetProcAddress(dll, name);
		if (func != NULL) {
			int a;
			a=func();
			std::cout <<name<<":"<< a << std::endl;
			FreeLibrary(dll);
		}
		else {
			std::cout << "The function isn't found." << std::endl;
		}
	}
	else {
		std::cout << "Failed to load dll file." << std::endl;
	}

}

void Helper::ls()
{
	std::string path = pwd();
	for (auto & p : std::experimental::filesystem::v1::directory_iterator(path)) {
		std::cout << p << std::endl;
	}
}

void Helper::createA(std::string name)
{
	std::ofstream outfile(name);
}

void Helper::cdA(std::string path)
{
	LPSTR s = const_cast<char *>(path.c_str());
	SetCurrentDirectory(s);
}

std::string Helper::pwd()
{
	LPSTR cwd = _getcwd(0, 0);
	std::string working_directory(cwd);
	return working_directory;
}

//*********************************************************************

void Helper::trim(std::string &str)
{
	rtrim(str);
	ltrim(str);

}

void Helper::rtrim(std::string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

void Helper::ltrim(std::string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

std::vector<std::string> Helper::get_words(std::string &str)
{
	std::vector<std::string> words;
	std::istringstream strStream(str);
	std::copy(std::istream_iterator<std::string>(strStream),
		std::istream_iterator<std::string>(),
		std::back_inserter(words));

	return words;
}

int main()
{
	std::cout << R"(
	Welcome to Windows Shell v1-Reverse shell!
	Here you can use bash commands. 
	Commands supported:
	*pwd
	*ls
	*cd
	*create
	*you can load the dll file "secret"

)";
	Helper h;
	while (1) {
		std::cout << ">>>>>>>";
		std::string input;
		std::string output;
		std::getline(std::cin, input);
		h.trim(input);
		std::vector<std::string> words;
		words=h.get_words(input);
		if (words[0] == "pwd") {
			output=h.pwd();
			std::cout << output << std::endl;
		}
		if (words[0] == "cd") {
			h.cdA(words[1]);
		}
		if (words[0] == "create") {
			h.createA(words[1]);
		}
		if (words[0] == "ls") {
			h.ls();
		}
		if (words[0] == "secret") {
			char* name = _strdup("TheAnswerToLifeTheUniverseAndEverything");
			h.secretA(name);
		}
	

	}
	system("pause");
	return 0;
}
