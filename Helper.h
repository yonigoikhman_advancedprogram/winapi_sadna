#ifndef _HELPER_H
#define _HELPER_H

#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>
#include <Windows.h>
#include <fcntl.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <windows.h>
// this is service class with static functions
class Helper
{
public:
	void secretA(char * name);
	void ls();
	void createA(std::string name);
	void cdA(std::string path);
	std::string pwd();
	// Remove whitespace from the begining and the end of the string
	static void trim(std::string &str);

	// Get a vector of strings, and return a vector of words 
	// ("for example" vec[0]:"for" vec[1]:"example"
	static std::vector<std::string> get_words(std::string &str);

private:
	// Remove whitespace from the end of the string
	static void rtrim(std::string &str);

	// Remove whitespace from the beginning of the string
	static void ltrim(std::string &str);


};

#endif